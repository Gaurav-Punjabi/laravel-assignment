<?php


namespace App;


class SubjectService
{
    public function store($name, $current_user_id)
    {
        return Subject::create([
            "name" => $name,
            "created_by" => $current_user_id
        ]);
    }

    public function update($id, $name, $current_user_id)
    {
        $subject = Subject::findOrFail($id);
        $subject->update([
            "name" => $name,
            "updated_by" => $current_user_id
        ]);
    }

    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();
    }
}

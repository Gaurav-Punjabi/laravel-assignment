<?php

namespace Tests\Unit;

use App\Subject;
use App\SubjectService;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubjectServiceTest extends TestCase
{
    protected $subjectService;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->subjectService = new SubjectService();
    }

    /**
     * @test
     */
    public function itShouldAddASubject()
    {
        // Creating a dummy user
        $user = factory(User::class)->create();
        // Fetching the old row count
        $old_count = Subject::all()->count();

        $name = "Subject A";

        $subject = $this->subjectService->store($name, $user->id);

        $new_count = Subject::all()->count();;

        $this->assertInstanceOf(Subject::class, $subject);
        $this->assertEquals($name, $subject->name);
        $this->assertEquals($old_count + 1, $new_count);
    }

    /**
     * @test
     */
    public function itShouldUpdateSubject()
    {
        $user = factory(User::class)->create();
        $subject = factory(Subject::class)->create();

        $new_name = "Subject B";
        $this->subjectService->update($subject->id, $new_name, $user->id);
        $new_subject = Subject::findOrFail($subject->id);

        $this->assertEquals($new_subject->name, $new_name);
    }


    /**
     * @test
     */
    public function itShouldDeleteSubject()
    {
        $subject = factory(Subject::class)->create();
        // Fetching the old row count
        $old_count = Subject::all()->count();

        $this->subjectService->destroy($subject->id);

        $new_count = Subject::all()->count();

        $this->assertEquals($new_count, $old_count - 1);
    }
}
